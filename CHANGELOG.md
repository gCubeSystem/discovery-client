This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog

## [v2.0.0]

- Moved to jakarta

## [v1.0.3] - 2022-06-06

- Added logs library as test dependency

## [v1.0.2] - 2022-06-06

- Commit for release

